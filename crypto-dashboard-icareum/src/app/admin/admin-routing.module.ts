import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {RouteConstants} from "../utility/constants/routes";
import {AuthGaurd} from "../_guards/auth.gaurds";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {TransactionHistoryComponent} from "./transaction-history/transaction-history.component";
import {SendComponent} from "./send/send.component";
import {BalancesComponent} from "./balances/balances.component";
import {ReferralLinkComponent} from "./referral-link/referral-link.component";
import {AddressInfoComponent} from "./address-info/address-info.component";
import {BuyICRMComponent} from "./buy-icrm/buy-icrm.component";
import {PresaleComponent} from "./presale/presale.component";
import {UserProfileComponent} from "./user-profile/user-profile.component";
import {SettingsComponent} from "./settings/settings.component";
import {EtheriumComponent} from "./header-sidebar/etherium/etherium.component";
import {LoadWalletComponent} from "./header-sidebar/load-wallet/load-wallet.component";
import {BuyTokensComponent} from "./header-sidebar/buy-tokens/buy-tokens.component";

const routes: Routes = [
  {
    path: RouteConstants.DASHBOARD,
    component: DashboardComponent,
    canActivate: [AuthGaurd]
  },
  {
    path: RouteConstants.TRANSACTION_HISTORY,
    component: TransactionHistoryComponent,
    canActivate: [AuthGaurd]
  },
  {
    path: RouteConstants.SEND,
    component: SendComponent,
    canActivate: [AuthGaurd]
  },
  {
    path: RouteConstants.BALANCES,
    component: BalancesComponent,
    canActivate: [AuthGaurd]
  },
  {
    path: RouteConstants.REFERRAL_LINK,
    component: ReferralLinkComponent,
    canActivate: [AuthGaurd]
  },
  {
    path: RouteConstants.ADDRESS_INFO,
    component: AddressInfoComponent,
    canActivate: [AuthGaurd]
  },
  {
    path: RouteConstants.PRESALE,
    component: PresaleComponent,
    canActivate: [AuthGaurd]
  },
  {
    path: RouteConstants.BUY_ICRM,
    component: BuyICRMComponent,
    canActivate: [AuthGaurd]
  },
  {
    path: RouteConstants.USER_PROFILE,
    component: UserProfileComponent,
    canActivate: [AuthGaurd]
  },
  {
    path: RouteConstants.SETTINGS,
    component: SettingsComponent,
    canActivate: [AuthGaurd]
  },
  {
    path: RouteConstants.GET_ETHERIUM,
    component: EtheriumComponent,
    canActivate: [AuthGaurd]
  },
  {
    path: RouteConstants.WALLET,
    component: LoadWalletComponent,
    canActivate: [AuthGaurd]
  },
  {
    path: RouteConstants.BUY_TOKEN,
    component: BuyTokensComponent,
    canActivate: [AuthGaurd]
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})

export class AdminRoutingModule {}
