import {Component, OnInit} from '@angular/core';
import {SharedService} from "../../utility/shared-service/shared.service";
import {Web3Service} from "../../utility/shared-service/web3.service";
import {slideUp} from "../animation";
import {APIManager} from "../../utility/shared-service/apimanager.service";
import {API} from "../../utility/constants/api";

@Component({
  selector: 'app-balances',
  templateUrl: './balances.component.html',
  styleUrls: ['./balances.component.css'],
  animations: [slideUp]
})
export class BalancesComponent implements OnInit {
  etherBalance: string = "0";
  loaderEther: boolean = true;

  tokenBalance: string = "0";
  loaderToken: boolean = true;

  referrerAdr: string = "";

  constructor(private sharedService: SharedService, private web3Service: Web3Service, private apiManager: APIManager) {

  }

  ngOnInit() {
    this.balanceRefresh()
    // var user = this.sharedService.getUser();
    //   let params = {
    //   code: user.referredBy
    // }
    // this.apiManager.postAPI(API.REFERRER_ADDRESS, params).subscribe(response=> {
    //   console.log(response.payload.address);
    // }, error => {
    //   this.referrerAdr = "";
    // });
  }

  checkAccountBalance(account) {
    this.web3Service.getEtherAccountBalance(account).subscribe(result=> {
      this.loaderEther = false;
      this.etherBalance = result;
    }, error=> {
      this.etherBalance = error;
      //console.log(error);
    });

    this.web3Service.getTokenBalance(account).subscribe(result=> {
      this.tokenBalance = Number(result).toFixed(0);
      this.loaderToken = false;
    }, error=> {
      this.tokenBalance = error;
      //console.log(error);
    });


  }

  balanceRefresh() {
    this.loaderEther = true;
    this.loaderToken = true;
    this.sharedService.trackMixPanelEvent("Refresh Balance");
    this.etherBalance = "0";
    this.tokenBalance = "0";
    this.checkAccountBalance(this.sharedService.getWalletAddress.address);
  }



}
